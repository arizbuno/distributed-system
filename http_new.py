import sys
import socket
import datetime
import random
import json
import yaml
import requests

response_status_msg = {
    200: "OK",
    302: "Found",
    400: "Bad Request",
    404: "Not Found",
    405: "Method Not Allowed",
    501: "Not Implemented"
}

response_content_type_msg = {
    0: "",
    "html": "text/html",
    "css": "text/css",
    "img": "image/jpg",
    "plain": "text/plain; charset=UTF-8",
    "json": "application/json",
    "yaml": "text/yaml"
}

def handler(method, path, protocol, request=None, time=None):
    status = 0
    content_type = 0
    content_length = 0
    body = b""

    path_list = path.split("/")

    if protocol != "HTTP/1.1" and protocol != "HTTP/1.0":
        status = 400
        return response_builder(status)
    elif method != "GET" and method != "POST":
        status = 501
        return response_builder(status)
    else:
        if path == '/':
            status = 302
            return response_builder(status, location="/hello-world")

        elif method == "GET":
            status = 200

            if path == "/hello-world":
                print("html")
                html = open('hello-world.html', 'r')
                body = html.read()
                html.close()
                body = str(body).replace('__HELLO__', 'World').encode('utf-8')
                content_type = "html"

            elif path == "/style":
                print("Style")
                css = open('style.css', 'rb')
                body = css.read()
                css.close()
                content_type = "css"

            elif path == "/background":
                print("Background")
                background = open('background.jpg', 'rb')
                body = background.read()
                background.close()
                content_type = "img"

            elif "info" in path:
                type_request = path.split('?type=')[1]
                if type_request == 'time':
                    print("Time")
                    response = str(datetime.datetime.now())
                elif type_request == 'random':
                    print("Random")
                    response = str(random.randint(0, 10))
                else:
                    response = "No Data"

                body = response.encode('utf-8')
                content_type = "plain"

            elif path_list[1] == "api":
                status = 404
                content_type = "json"
                try:
                    service = path_list[2]
                    if service == "plusone":
                        plus = path_list[3]
                        return plusone(plus)
                    elif service == "spesifikasi.yaml":
                        print("Spesifikasi")
                        spec = open('spesifikasi.yaml', 'r')
                        body = spec.read()
                        spec.close()
                        content_type = "yaml"
                        status = 200
                        body = body.encode('utf-8')
                        content_length = sys.getsizeof(body)
                        return response_builder(status, content_length, body, content_type)
                except IndexError:
                    pass
                body = error_msg_builder(status)
                body = body.encode('utf-8')

            else:
                status = 404
                return response_builder(status)
            
        elif method == "POST":

            if path == "/hello-world":
                lines_list = request.splitlines()
                urlencoded = False
                name = ""
                for line in lines_list:
                    if 'Content-Type: application/x-www-form-urlencoded' in line:
                        urlencoded = True
                    if 'name' in line:
                        name = line.split('=')[1]
                if urlencoded:
                    html = open('hello-world.html', 'r')
                    body = html.read()
                    html.close()
                    body = str(body).replace('__HELLO__', name).encode('utf-8')
                    status = 200
                    content_type = "html"
                else:
                    status = 400
                    return response_builder(status)

            else:
                status = 404
                try:
                    path_list = path.split('/')
                    # _, path, service = path.split('/')
                    path = path_list[1]
                    if path == "api":
                        service = path_list[2]
                        print(path, service)
                        if service == "hello":
                            return hello(request, time)
                    else:
                        pass
                except IndexError:
                    pass
                body = error_msg_builder(status)
                body = body.encode('utf-8')
                content_length = sys.getsizeof(body)
                return response_builder(status, content_length, body, 'json')

        content_length = sys.getsizeof(body)
        return response_builder(status, content_length, body, content_type)
            

def response_builder(status, content_length=0, body=None, content_type=0, location=None):
    header = "HTTP/1.1 " + str(status) + " " + response_status_msg[status] + "\n"
    if content_length > 0:
        header += "Content-Length: " + str(content_length - 33) + "\n"
    else:
        header += "Content-Length: " + str(content_length) + "\n"
    
    if content_type != 0:
        header += "Content-Type: " + response_content_type_msg[content_type] + "\n"
    if location != None:
        header += "Location: " + location + "\n"

    header += "Connection: close\n\n"
    response = header.encode('utf-8')
    if body != None:
        print(body)
        response += body
    
    return response


def apiversion():
    with open("spesifikasi.yaml", 'r') as stream:
        try:
            return yaml.load(stream)['info']['version']
        except yaml.YAMLError as exc:
            print(exc)


def hello(request, time):
    status = 200

    greet = "Day"
    try:
        greet = requests.get('http://172.22.0.222:5000')
        print(greet)
        greet = json.loads(greet.text)['state']
        print(greet)
    except requests.exceptions.ConnectionError:
        pass

    lines_list = request.splitlines()
    is_json = False
    for line in lines_list:
        if 'Content-Type: application/json' in line:
            is_json = True
            break
    if is_json:
        try:
            json_body = json.loads(request.split('\r\n')[-1])
            print(json_body)
        except json.decoder.JSONDecodeError:
            status = 400
        try:
            hit_count = 0
            with open('count.json', 'r+') as f:
                data = json.load(f)
                data['count'] =  data['count'] + 1
                hit_count = data['count']
                f.seek(0)
                json.dump(data, f, indent=4)
                f.truncate()
            name = json_body["request"]
            body = {
                "apiversion": apiversion(),
                "count": hit_count,
                "currentvisit": time,
                "response": "Good " + greet + ", " + name
            }
            body = json.dumps(body)
            print(body)
            body = body.encode('utf-8')
            content_length = sys.getsizeof(body)
            print("content_length", content_length)
            return response_builder(status, content_length, body, 'json')
        except KeyError:
            status = 400
    else:
        status = 405
    body = error_msg_builder(status)
    body = body.encode('utf-8')
    content_length = sys.getsizeof(body)
    print("content_length", content_length)
    return response_builder(status, content_length, body, 'json')


def plusone(plus):
    status = 200
    try:
        plus = int(plus)
        ret = plus + 1
        body = {
            "apiversion": apiversion(),
            "plusoneret": ret
        }
        body = json.dumps(body)
        print(body)
        body = body.encode('utf-8')
        content_length = sys.getsizeof(body)
        print("content_length", content_length)
        return response_builder(status, content_length, body, 'json')
    except ValueError:
        status = 404
    body = error_msg_builder(status)
    body = body.encode('utf-8')
    content_length = sys.getsizeof(body)
    print("content_length", content_length)
    return response_builder(status, content_length, body, 'json')


def error_msg_builder(status):
    detail_msg = {
        400: "'request' is a required property",
        404: "The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.",
        405: "Must use Content-Type: application/json to use this method"
    }
    body = {
        "detail": detail_msg[status],
        "status": status,
        "title": response_status_msg[status]
    }
    print(body)
    return json.dumps(body)


HOST, PORT = '0.0.0.0', 80

# Menginisiasi socket
listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
print('Serving HTTP on port %s ...' % PORT)

while True:
    try:
        connection, address = listen_socket.accept()
        request = connection.recv(1024).decode('utf-8')
        time = str(datetime.datetime.now())
        print(request)

        try:
            # Mengatur agar request bisa di parse
            request_line = request.split('\r\n')[0]
            request_list = request_line.split(' ')
            print(request_list)

            method = request_list[0]
            path = request_list[1]
            protocol = request_list[2]

            if method == "POST":
                response = handler(method, path, protocol, request, time)
            else:
                response = handler(method, path, protocol)

            connection.send(response)
            connection.close()
        except IndexError:
            pass

    except KeyboardInterrupt:
        pass
